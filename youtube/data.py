selected = []
selectable = []

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube</h1>
    <h2>Seleccionados</h2>
      <ul>
      {selected}
      </ul>
    <h2>Seleccionables</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='{link}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'> 
          <input type='submit' value='{action}'>
        </form>
      </li>
"""
